package com.example.myapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity1 extends Activity implements View.OnClickListener{

    //UI Object
    private TextView ms_topbar_title;
    private TextView ms_tab_home;
    private TextView ms_tab_explore;
    private TextView ms_tab_card_travel;
    private TextView ms_tab_settings;
    private FrameLayout ms_content;

    //Fragment Object
    private MsFragment fg1;
    private MsFragmentSetting fg4;
    private MsFragmentexplore fg2;
    private MsFragmentDiary fg3;
    private FragmentManager fManager;

    ScaleAnimation scaleAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main1);
        fManager = getFragmentManager();
        bindViews();

        ms_tab_home.performClick();   //模拟一次点击，既进去后选择第一项


    }

    //UI组件初始化与事件绑定
    private void bindViews() {
        ms_topbar_title = (TextView) findViewById(R.id.ms_topbar_title);
        ms_tab_home = (TextView) findViewById(R.id.ms_tab_home);
        ms_tab_explore = (TextView) findViewById(R.id.ms_tab_explore);
        ms_tab_card_travel = (TextView) findViewById(R.id.ms_tab_card_travel);
        ms_tab_settings = (TextView) findViewById(R.id.ms_tab_settings);
        ms_content = (FrameLayout) findViewById(R.id.ms_content);

        ms_tab_home.setOnClickListener(this);
        ms_tab_explore.setOnClickListener(this);
        ms_tab_card_travel.setOnClickListener(this);
        ms_tab_settings.setOnClickListener(this);
    }

    //重置所有文本的选中状态
    private void setSelected(){

        ms_tab_home.setSelected(false);
        ms_tab_explore.setSelected(false);
        ms_tab_card_travel.setSelected(false);
        ms_tab_settings.setSelected(false);
    }

    //隐藏所有 Fragment
    private void hideAllFragment(FragmentTransaction fragmentTransaction){
        if(fg1 != null) fragmentTransaction.hide(fg1);
        if(fg2 != null) fragmentTransaction.hide(fg2);
        if(fg3 != null) fragmentTransaction.hide(fg3);
        if(fg4 != null) fragmentTransaction.hide(fg4);
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction fTransaction = fManager.beginTransaction();
        hideAllFragment(fTransaction);
        switch (v.getId()){
            case R.id.ms_tab_home:
                setSelected();
                ms_tab_home.setSelected(true);
                if(fg1 == null){

                    fg1 = new MsFragment();
                    fTransaction.add(R.id.ms_content,fg1);
                }else{
                    fTransaction.show(fg1);
                }
                break;
            case R.id.ms_tab_explore:
                setSelected();
                ms_tab_explore.setSelected(true);
                if(fg2 == null){
                    fg2 = new MsFragmentexplore();
                    fTransaction.add(R.id.ms_content,fg2);
                }else{
                    fTransaction.show(fg2);
                }
                break;
            case R.id.ms_tab_card_travel:
                setSelected();
                ms_tab_card_travel.setSelected(true);
                if(fg3 == null){
                    fg3  = new MsFragmentDiary();
                    fTransaction.add(R.id.ms_content,fg3);
                }else{
                    fTransaction.show(fg3);
                }
                break;
            case R.id.ms_tab_settings:
                setSelected();
                ms_tab_settings.setSelected(true);
                if(fg4 == null){
                    fg4 = new MsFragmentSetting();
                    fTransaction.add(R.id.ms_content,fg4);

                }else{
                    fTransaction.show(fg4);
                }
                break;
        }

        fTransaction.commit();
    }

    public void onClickButton(View view) {
        view = (ImageView) findViewById(R.id.imgzoomin);
        scaleAnimation = (ScaleAnimation) AnimationUtils.loadAnimation(MainActivity1.this, R.anim.zoom_in);
        view.startAnimation(scaleAnimation);
    }


}

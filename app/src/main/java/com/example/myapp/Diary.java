package com.example.myapp;

public class Diary {
    private String editText;
    private String imageView;
    private String date;
    public Diary(String editText , String imageView, String date) {
        this.editText = editText;
        this.imageView = imageView;
        this.date = date;
    }
    public Diary(){}
    public String getEditText() {
        return editText;
    }

    public void setEditText(String editText) {
        this.editText = editText;
    }

    public String getImageView() {
        return imageView;
    }

    public void setImageView(String imageView) {
        this.imageView = imageView;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getDate() {
        return date;
    }

    public String toString(){
        return "Date: "+date + "\n" + "Text: " + this.editText + "\n" +"Image URL: " + this.imageView;
    }


}

package com.example.myapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Add_diary extends AppCompatActivity {

    private static final int REQUEST_READ_EXTERNAL_STORAGE = 0x1000;
    private static final int REQUEST_GALLERY = 0x1001;

    private LinearLayout contentLayout;
    private Button saveButton;

    private Date date = new Date();
    private ArrayList<Pair<View, String>> contents = new ArrayList<>();
    private String getDate;

    private Uri Imguri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_diary);
        contentLayout = (LinearLayout) findViewById(R.id.content_layout);
        saveButton = (Button) findViewById(R.id.save_button);

        setView();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    Map<String, Object> newrecord = new HashMap<String, Object>();
                    DatabaseReference myRef = database.getReference("contacts");
//
//                    myRef.setValue("Hello, World!");
//                    Log.d("myTag", "This is my message");
//                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < contents.size(); i++) {
                        Pair<View, String> content = contents.get(i);
                        if (content.first instanceof EditText) {
//                            test = ((EditText) content.first).getText().toString();
                            newrecord.put("editText",((EditText) content.first).getText().toString());
                            Log.v("DiaryEditActivity", "[saveDiary1] " + ((EditText) content.first).getText().toString());
                        } else if (content.first instanceof ImageView) {
                            newrecord.put("imageView",content.second);
                            Log.v("DiaryEditActivity", "[saveDiary2] " + content.second);
                        }
                    }

                    newrecord.put("date",getDate);
                    Log.v("DiaryEditActivity", "[saveDiary3] " + getDate);
                    myRef.push().setValue(newrecord).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(Add_diary.this, "Add Success", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent();
                                intent.setClass(Add_diary.this, MainActivity1.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(Add_diary.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }


    private void setView() {
        setTitle(new SimpleDateFormat("yyyy, MM/dd").format(date));
        getDate = new SimpleDateFormat("yyyy, MM/dd").format(date);
        addEditText("");
    }

    private void addEditText(String text) {
        EditText editText = new EditText(this);
        editText.setTextColor(0xff4a4a4a);
        editText.setTextSize(18);
        editText.setText(text);
        editText.setBackgroundColor(Color.TRANSPARENT);
        editText.requestFocus();

        contents.add(new Pair<View, String>(editText, text));
        contentLayout.addView(editText);
    }

    private void addImageView(Uri uri) {
        if (contents.get(contents.size() - 1).first instanceof EditText) {
            EditText editText = (EditText) contents.get(contents.size() - 1).first;
            if (editText.getText().toString().length() == 0) {
                contents.remove(contents.size() - 1);
                contentLayout.removeView(editText);
            }
        }

        ImageView imageView = new ImageView(this);
        imageView.setImageURI(uri);
        imageView.setPadding(10, 10, 10, 10);

        contents.add(new Pair<View, String>(imageView, uri.toString()));
        contentLayout.addView(imageView);
        Imguri = uri;
        addEditText("");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.diary_edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_set_time) {
            setupTime();
        } else if (item.getItemId() == R.id.action_add_photo) {
            openGallery();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupTime() {
        final Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                date.setYear(year - 1900);
                date.setMonth(month);
                date.setDate(day);
                date.setHours(1);
                date.setMinutes(1);
                date.setSeconds(1);

                setTitle(new SimpleDateFormat("yyyy, MM/dd").format(date));
                getDate = new SimpleDateFormat("yyyy, MM/dd").format(date);
            }

        }, year, month, day).show();
    }
    private void openGallery() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(this)
                        .setMessage("我真的沒有要做壞事, 給我權限吧?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(Add_diary.this,
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                        REQUEST_READ_EXTERNAL_STORAGE);
                            }
                        })
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_READ_EXTERNAL_STORAGE);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_GALLERY);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_EXTERNAL_STORAGE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_GALLERY);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_GALLERY) {

                Uri uri = data.getData();
                addImageView(uri);
            }
        }
    }
}
